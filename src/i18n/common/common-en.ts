export default {
  USER_CENTER: 'User Center',
  LOGOUT: 'Logout',
  FOOTER: {
    ATOM_TEXT:
      'openEuler is an open source project incubated and operated by the OpenAtom Foundation.',
    ATOM_PC: '/atom-pc.png',
    ATOM_MO: '/atom-mo.png',
    MAIL: 'contact@openeuler.io',
    COPY_RIGHT: 'Copyright © {year} openEuler. All rights reserved.',
    LICENSED_1: 'Licensed under',
    LICENSED_2: 'the MulanPSL2',
    QR_CODE: 'WeChat Subscription',
    QR_ASSISTANT: 'WeChat Assistant',
  },
};

export default {
  SIG_LANDSCAPE: [
    {
      CATEGORY_NAME: 'Code Repository Management/Technology Innovation',
    },
    {
      CATEGORY_NAME: 'Community Governance and Operations',
    },
  ],
};
